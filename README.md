# Proj0-Hello 
-------------
Trivial project to exercise version control, turn-in, and other
mechanisms. Contains a python file which prints a message from 
a secret credentials configuration file. 

Author: Juno Mayer (Using a base by Ram Durairajan)
Questions? Contact me at junom@cs.uoregon.edu